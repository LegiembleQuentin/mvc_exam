<div class="wrap">
    <h1>Ajouter une salle</h1>
    <form action="" method="post" novalidate>
        <?php
        echo $form->label('title', 'Nom');
        echo $form->input('title');
        echo $form->error('title');

        echo $form->label('maxuser', 'Nombre de places');
        echo $form->input('maxuser');
        echo $form->error('maxuser');

        echo $form->submit('submitted', 'Ajouter');
        ?>
    </form>
</div>