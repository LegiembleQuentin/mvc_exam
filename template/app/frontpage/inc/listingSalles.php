<section id="salles" class="wrap">
    <h1>Salles</h1>

    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Places</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($salles as $salle){ ?>
            <tr>
                <td><?= $salle->title ?></td>
                <td><?= $salle->maxuser ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="<?= $view->path('add-salle'); ?>">
        <p>Ajouter une salle</p>
    </a>
</section>