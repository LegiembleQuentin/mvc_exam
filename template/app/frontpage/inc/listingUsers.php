<section id="users" class="wrap">
    <h1>Utilisateurs</h1>

    <table>
        <thead>
        <tr>
            <th>Nom</th>
            <th>Email</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($users as $user){ ?>
            <tr>
                <td><?= strtoupper($user->name) ?></td>
                <td><?= $user->email ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="<?= $view->path('add-user'); ?>">
        <p>Ajouter un utilisateur</p>
    </a>
</section>