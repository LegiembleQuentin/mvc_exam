<section id="creneaux" class="wrap">
    <h1>Salles</h1>

    <table>
        <thead>
        <tr>
            <th>Salle</th>
            <th>Début</th>
            <th>Durée</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($creneaux as $creneau){ ?>
            <tr>
                <td><?= $creneau->salle_name ?></td>
                <td>Le <?= $view->formatDate($creneau->startAt) ?></td>
                <td><?= $creneau->nbre_hours ?></td>
                <td><a href="<?= $view->path('creneaux/'.$creneau->id) ?>">Voir plus</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="<?= $view->path('add-creneau'); ?>">
        <p>Ajouter un creneau</p>
    </a>
</section>