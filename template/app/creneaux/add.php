<div class="wrap">
    <h1>Ajouter un creneau</h1>
    <form action="" method="post" novalidate>
        <?php
        echo $form->label('salle');
        echo $form->selectEntity('salle', $salles, 'title');
        echo $form->error('salle');

        echo $form->label('startAt', 'Début');
        echo $form->input('startAt', 'datetime-local');
        echo $form->error('startAt');

        echo $form->label('nbreHours', 'Durée (en heures)');
        echo $form->input('nbreHours');
        echo $form->error('nbreHours');

        echo $form->submit('submitted', 'Ajouter');
        ?>
    </form>
</div>