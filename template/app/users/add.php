<div class="wrap">
    <h1>Ajouter un utilisateur</h1>
    <form action="" method="post" novalidate>
        <?php
        echo $form->label('name', 'Nom');
        echo $form->input('name');
        echo $form->error('name');

        echo $form->label('email');
        echo $form->input('email', 'Email');
        echo $form->error('email');

        echo $form->submit('submitted', 'Ajouter');
        ?>
    </form>
</div>