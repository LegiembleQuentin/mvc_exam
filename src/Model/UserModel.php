<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class UserModel extends AbstractModel
{
    protected static $table = 'user';

    protected $id;
    protected $name;
    protected $email;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    public static function getUserByEmail($email)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE email = ?",[$email],get_called_class(),true);
    }

    public static function insert($post) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (name, email) VALUES (?,?)", array($post['name'], $post['email']));
    }
}