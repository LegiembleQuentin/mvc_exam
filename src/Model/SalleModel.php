<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel
{
    protected static $table = 'salle';

    protected $id;
    protected $title;
    protected $maxuser;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->maxuser;
    }

    public static function getUserByTitle($title)
    {
        return App::getDatabase()->prepare("SELECT * FROM " . self::getTable() . " WHERE title = ?",[$title],get_called_class(),true);
    }

    public static function insert($post) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (title, maxuser) VALUES (?,?)", array($post['title'], $post['maxuser']));
    }

}