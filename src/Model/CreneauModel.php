<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauModel extends AbstractModel
{
    protected static $table = 'creneau';

    protected $id;
    protected $id_salle;
    protected $start_at;
    protected $nbrehours;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdSalle()
    {
        return $this->id_salle;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->start_at;
    }

    /**
     * @return mixed
     */
    public function getNbrehours()
    {
        return $this->nbrehours;
    }

    public static function insert($post) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO " . self::$table . " (id_salle, start_at, nbrehours) VALUES (?,?,?)", array($post['salle'], $post['startAt'], $post['nbreHours']));
    }

    public static function insertUser($post, $id) : void
    {
        App::getDatabase()->prepareInsert("INSERT INTO creneau_user (id_creneau, id_user, created_at) VALUES (?,?,NOW())", array($id, $post['user']));
    }

    public static function getParticipant($idUser)
    {
        return App::getDatabase()->prepare("SELECT * FROM creneau_user WHERE id_user = ?", [$idUser], get_called_class(), true);
    }

//    public static function getNbParticipants($idCreneau) {
//        return App::getDatabase()->prepare("SELECT COUNT(*) FROM creneau_user WHERE id_creneau = ?", [$idCreneau], get_called_class(), true);
//    }
//
//    public static function getMaxPlaces($id)
//    {
//        $salle = App::getDatabase()->prepare("SELECT id_salle FROM " . self::getTable() . " WHERE id = ?", [$id], get_called_class(), true);
//        $idSalle = $salle->id_salle;
//        return App::getDatabase()->prepare("SELECT maxuser FROM salle WHERE id = ?", [$idSalle], get_called_class(), true);
//    }

    public static function getAllCreneauxInfos()
    {
        return App::getDatabase()->query("SELECT c.id AS id, s.title AS salle_name, c.start_at AS start_at, c.nbrehours AS nbre_hours 
                                FROM " . self::getTable() . " AS c 
                                LEFT JOIN salle AS s ON c.id_salle = s.id
                                ORDER BY start_at ASC",get_called_class());
    }

    public static function getCreneauInfos($creneauId)
    {
        return App::getDatabase()->prepare("SELECT c.id AS id, s.title AS salle_name, c.start_at AS start_at, c.nbrehours AS nbre_hours 
                                FROM " . self::getTable() . " AS c 
                                LEFT JOIN salle AS s ON c.id_salle = s.id
                                WHERE c.id = ?
                                ORDER BY start_at ASC", [$creneauId], get_called_class(), true);
    }


}