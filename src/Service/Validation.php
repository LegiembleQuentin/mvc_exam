<?php
namespace App\Service;

use DateTime;

class Validation
{
    protected $errors = array();

    public function IsValid($errors)
    {
        foreach ($errors as $key => $value) {
            if(!empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * emailValid
     * @param email $email
     * @return string $error
     */

    public function emailValid($email)
    {
        $error = '';
        if(empty($email) || (filter_var($email, FILTER_VALIDATE_EMAIL)) === false) {
            $error = 'Adresse email invalide.';
        }
        return $error;
    }

    /**
     * textValid
     * @param POST $text string
     * @param title $title string
     * @param min $min int
     * @param max $max int
     * @param empty $empty bool
     * @return string $error
     */

    public function textValid($text, $title, $min = 3,  $max = 50, $empty = true)
    {

        $error = '';
        if(!empty($text)) {
            $strtext = strlen($text);
            if($strtext > $max) {
                $error = 'Votre ' . $title . ' est trop long.';
            } elseif($strtext < $min) {
                $error = 'Votre ' . $title . ' est trop court.';
            }
        } else {
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }
        return $error;

    }

    function validateNumber($number, $title, $min = 1, $max = 4, $empty = true) {
        $error = '';

        if(!empty($number)) {
            if (!is_numeric($number)) {
                $error = 'Votre ' . $title . ' n\'est pas un nombre valide.';
            } elseif(strlen($number) < $min) {
                $error = 'Votre ' . $title . ' est trop petite.';
            } elseif(strlen($number) > $max) {
            $error = 'Votre ' . $title . ' est trop grande.';
        }

        }else{
            if($empty) {
                $error = 'Veuillez renseigner un ' . $title . '.';
            }
        }

        return $error;
    }

    public function validateDateTimeLocal($datetime, $title) {
        $error = '';

        $dateTimeObj = DateTime::createFromFormat('Y-m-d\TH:i', $datetime);
        if (!$dateTimeObj || $dateTimeObj->format('Y-m-d\TH:i') !== $datetime) {
            $error = 'Votre ' . $title . ' n\'est pas valide.';
        }

        return $error;
    }
}
