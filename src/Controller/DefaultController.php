<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use Core\Kernel\AbstractController;
use App\Model\UserModel;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $users = UserModel::all();
        $salles = SalleModel::all();
        $creneaux = CreneauModel::getAllCreneauxInfos();

        $this->render('app.frontpage.frontpage',array(
            'users'=>$users,
            'salles'=>$salles,
            'creneaux'=>$creneaux,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
