<?php

namespace App\Controller;

use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class UserController extends AbstractController
{
    public function add(){
        $errors = [];

        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateUser($v,$post);

            if($v->IsValid($errors)){
                UserModel::insert($post);

//                $this->addFlash('success', 'L\'utilisateur a bien été enregistré.');
                $this->redirect('');
            }
        }

        $form = new Form($errors);

        $this->render('app.users.add',[
            'form'=>$form,
        ]);
    }

    private function validateUser($v,$post)
    {
        $errors = [];
        $errors['name'] = $v->textValid($post['name'], 'Nom', 2, 255);
        $errors['email'] = $v->emailValid($post['email'], 'email');
        if (empty($errors['email'])){
            $testuser = UserModel::getUserByEmail($post['email']);
            if (!empty($testuser)){
                $errors['email'] = 'Cet email correspond déjà à un utilisateur existant';
            }
        }

        return $errors;
    }



}