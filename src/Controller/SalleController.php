<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class SalleController extends AbstractController
{
    public function add(){
        $errors = [];

        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateSalle($v,$post);

            if($v->IsValid($errors)){
                SalleModel::insert($post);
//
//                $this->addFlash('success', 'Votre salle a bien été enregistrée.');
                $this->redirect('');
            }
        }

        $form = new Form($errors);

        $this->render('app.salles.add',[
            'form'=>$form,
        ]);
    }

    private function validateSalle($v,$post)
    {
        $errors = [];
        $errors['title'] = $v->textValid($post['title'], 'nom', 2, 255);
        if (empty($errors['title'])){
            $testsalle = SalleModel::getUserByTitle($post['title']);
            if (!empty($testsalle)){
                $errors['title'] = 'Cette salle existe déjà.';
            }
        }
        $errors['maxuser'] = $v->validateNumber($post['maxuser'], 'nombre de places', 1, 4);


        return $errors;
    }

}