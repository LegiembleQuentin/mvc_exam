<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UserModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class CreneauController extends AbstractController
{
    public function add(){
        $salles = SalleModel::all();
        $errors = [];

        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateCreneau($v,$post);

            if($v->IsValid($errors)){
                CreneauModel::insert($post);

//                $this->addFlash('success', 'Votre créneau a bien été enregistré.');
                $this->redirect('');
            }
        }

        $form = new Form($errors);

        $this->render('app.creneaux.add',[
            'form'=>$form,
            'salles'=>$salles,
        ]);
    }

    public function single($id)
    {
        $creneau =  $this->getCreneauOr404($id);
        $users = UserModel::all();

        $errors = [];

        if(!empty($_POST['submitted'])){
            $post = $this->cleanXss($_POST);

            $v = new Validation();
            $errors = $this->validateNewUser($v,$post, $id);

            if($v->IsValid($errors)){
                CreneauModel::insertUser($post, $id);

//                $this->addFlash('success', 'Le participant a bien été ajouté.');
            }
        }

        $form = new Form($errors);

        $this->render('app.creneaux.single',[
            'creneau' => $creneau,
            'users' => $users,
            'form' => $form,
        ]);
    }

    private function validateCreneau($v,$post)
    {
        $errors = [];
        $verifSalle = SalleModel::findById($post['salle']);
        if (!$verifSalle){
            $errors['salle'] = 'La salle sélectionnée est invalide.';
        }

        $errors['startAt'] = $v->validateDateTimeLocal($post['startAt'], 'heure de début');
        $timestamp = strtotime($post['startAt']);
        $currentTimestamp = time();
        if ($timestamp < $currentTimestamp) {
            $errors['startAt'] = 'Votre heure de début est déjà passée';
        }

        $errors['nbreHours'] = $v->validateNumber($post['nbreHours'], 'durée', 1, 4);

        return $errors;
    }

    private function validateNewUser($v,$post, $id)
    {
        $errors = [];

//        $salle = CreneauModel::getMaxPlaces($id);
//        $maxPlaces = $salle->maxuser;
//        $nbUser = CreneauModel::getNbParticipants($id);

        $verifUser = UserModel::findById($post['user']);
        if (!$verifUser){
            $errors['user'] = 'Le participant sélectionnée est invalide.';
        }else {
            $verifParticipant = CreneauModel::getParticipant($post['user']);
            if (!empty($verifParticipant)){
                $errors['user'] = 'Cet utilisateur est déjà inscrit à ce créneau.';
            }
        }

        return $errors;
    }

    private function getCreneauOr404($id){
        $creneau = CreneauModel::getCreneauInfos($id);
        if(empty($creneau)){
            $this->abort404();
        }



        return $creneau;
    }


}